void openfile(char *filename, int *doflags);
void applyflags(char *filename, int const *flags);
typedef struct {
  int b;
  int e;
  int n;
  int s;
  int t;
  int v;
} Flags;

Flags parseFlags(const int *flags);
