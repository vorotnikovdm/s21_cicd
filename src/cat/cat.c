#include "cat.h"

#include <getopt.h>
#include <stdio.h>

void openfile(char *filename, int *doflags) {
  FILE *fp;
  if ((fp = fopen(filename, "r")) != NULL) {
    applyflags(filename, doflags);
    fclose(fp);
  } else
    fprintf(stderr, "s21_cat: %s: No such file or directory\n", filename);
}

void applyflags(char *filename, int const *flags) {
  int count = 0, row = 0, c, tempcurr = -1;
  Flags parsedFlags = parseFlags(flags);
  FILE *fp;
  fp = fopen(filename, "r");
  while ((c = fgetc(fp)) != EOF) {
    int tempprev = tempcurr;
    tempcurr = c;
    if ((parsedFlags.s == 1) && ((tempprev == '\n') || (tempprev == -1)) &&
        (tempcurr == '\n'))
      count++;
    else
      count = 0;
    if (count >= 2) continue;
    if ((parsedFlags.b == 1) && ((tempprev == '\n') || (tempprev == -1)) &&
        (tempcurr != '\n')) {
      row++;
      printf("%6d\t", row);
    } else if ((parsedFlags.n == 1) && (parsedFlags.b != 1) &&
               ((tempprev == '\n') || (tempprev == -1)) && tempcurr) {
      row++;
      printf("%6d\t", row);
    }
    if ((parsedFlags.v == 1) && (c >= 0) && (c <= 31) && (c != 9) &&
        (c != 10)) {
      printf("^%c", c + 64);
      continue;
    }
    if ((parsedFlags.v == 1) && (c == 127)) {
      printf("^?");
      continue;
    }
    if ((parsedFlags.e == 1) && (c == '\n')) printf("$");
    if ((parsedFlags.t == 1) && (c == '\t')) {
      printf("^%c", c + 64);
      continue;
    } else
      printf("%c", c);
  }
  fclose(fp);
}

Flags parseFlags(const int *flags) {
  Flags result = {0};
  for (int q = 0; q < 8; q++) {
    if (flags[q] == 'b') result.b = 1;
    if (flags[q] == 'e') result.e = 1;
    if (flags[q] == 'n') result.n = 1;
    if (flags[q] == 's') result.s = 1;
    if (flags[q] == 't') {
      result.t = 1;
      result.v = 1;
    }
    if (flags[q] == 'v') result.v = 1;
    if (flags[q] == 'E') result.e = 1;
    if (flags[q] == 'T') result.t = 1;
  }
  return result;
}
